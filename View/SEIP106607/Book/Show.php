<?php
session_start();
use App\BITM\SEIP106607\Book\Book;
use App\BITM\SEIP106607\Utility\Utility;

include_once ("../../../vendor/autoload.php");
//var_dump($_POST);die();
$book= new Book();

$b = $book->Show($_POST['ID']);

//var_dump($b);die();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Your Book</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="../../../css/StyleSheet.css">
    </head>
    <body class="body" style="background-color: #9acfea">
        <table border='1' style="text-align: center">
             <thead class="TabHead">
                <tr>
                    <td>SL.NO.</td>
                    <td>Title</td>
                    <td>Author</td>
                    
                </tr>
            </thead>
            <tbody>
                <?php
                $i=0;
                
                   $i++; 
                
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $b['title'];?></td>
                    <td><?php echo $b['author'];?></td>
                </tr>
                
            </tbody>
        </table>
        <div>
            <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="Index.php"><input type="submit" name="Index" value="Back to Library"></a></br> 
        </div>
        
        
    </body>
</html>


