<?php
//ini_set("display_errors","on" );
//error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
use App\BITM\SEIP106607\Book\Book;
use App\BITM\SEIP106607\Message\Message;
include_once ("../../../vendor/autoload.php");

$book = new Book();
$books = $book->recycle();
//print_r($books);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Recycle Bin</title>
        <link rel="stylesheet" type="text/css" href="../../../css/StyleSheet.css">
    </head>
    <body class="body" style="background-color: #9acfea">
        
        <table border='1' style="text-align: center">
            <thead class="TabHead">
                <tr>
                    <td>SL.No.</td>
                    <td>Book Title</td>
                    <td>Author Name</td>
                    <td>Image</td>
                    <td colspan="2">Action</td>
                </tr>
            </thead>
            <tbody class="tabBody">
                <?php 
                $i = 0;
                foreach ($books as $book){
                    $i++;
                    ?>
                <tr>
                    
                    <td><?php echo $i;?></td>
                    <td><?php echo $book['title'];?></td>
                    <td><?php echo $book['author'];?></td>
                    <td><img src="../Resources/Image/<?php echo $book['image'];?>" height="50" width="50"/></td>
                    <td>
                        <form action="recover.php" method="post">
                            <input type="hidden" name="ID" value="<?php echo $book['ID'];?>"/>
                            <input type="submit" value="Recover"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="ID" value="<?php echo $book['ID'];?>"/>
                            <input type="submit" value="Parmanently Delete"/>
                        </form>
                    </td>
                    
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div>
            <a href="Index.php"><input type="submit" name="create" value="Back To List"></a> 
        </div>
    </body>
</html>