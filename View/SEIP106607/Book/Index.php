<?php
//ini_set("display_errors","on" );
//error_reporting(E_ALL & ~E_DEPRECATED);
session_start();
use App\BITM\SEIP106607\Book\Book;
use App\BITM\SEIP106607\Message\Message;
include_once ("../../../vendor/autoload.php");

$book = new Book();
$books = $book->Index();
//print_r($books);

?>
<!DOCTYPE html>
<html>
    <head>
        <title>All Book List's Is Here</title>
        <link rel="stylesheet" type="text/css" href="../../../css/StyleSheet.css">
    </head>
    <body class="body" style="background-color: #9acfea">
        <div style="margin-top: 2px; margin-bottom: 2px;">
            <span>Download as <a href="pdf.php">PDF</a> | <a href="01simple-download-xlsx.php">Excel</a></span><br/><br/>
        </div>
        <div class="warning">
            <?php
            echo Message::flash();
        ?>
        </div>
        <table border='1' style="text-align: center">
            <thead class="TabHead">
                <tr>
                    <td>SL.No.</td>
                    <td>Book Title</td>
                    <td>Author Name</td>
                    <td>Image</td>
                    <td colspan="5">Action</td>
                </tr>
            </thead>
            <tbody class="tabBody">
                <?php 
                $i = 0;
                foreach ($books as $book){
                    $i++;
                    ?>
                <tr>
                    
                    <td><?php echo $i;?></td>
                    <td><?php echo $book['title'];?></td>
                    <td><?php echo $book['author'];?></td>
                    <td><img src="../Resources/Image/<?php echo $book['image'];?>" height="50" width="50"/></td>
                    <td>
                        <form action="Show.php" method="post">
                            <input type="hidden" name="ID" value="<?php echo $book['ID'];?>"/>
                            <input type="submit" value="View"/>
                        </form>
                            
                    </td>
                    <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="ID" value="<?php echo $book['ID'];?>"/>
                            <input type="submit" value="Edit"/>
                         </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="ID" value="<?php echo $book['ID'];?>"/>
                            <input type="submit" value="Delete"/>
                        </form>
                    </td>
                    <td>
                        <form action="trash.php" method="post">
                            <input type="hidden" name="ID" value="<?php echo $book['ID'];?>"/>
                            <input type="submit" value="Trash"/>
                        </form>
                    </td>
                    <td>
                        <form action="mail.php" method="post">
                            <input type="hidden" name="ID" value="<?php echo $book['ID'];?>"/>
                            <input type="submit" value="Mail to a friend"/>
                        </form>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div>
            <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a> 
            <a href="recycle.php"><input type="submit" name="recycle" value="Recycle Bin"></a>
        </div>
    </body>
</html>