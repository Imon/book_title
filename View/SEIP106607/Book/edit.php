<?php


use App\BITM\SEIP106607\Book\Book;
use App\BITM\SEIP106607\Message\Message;
include_once ("../../../vendor/autoload.php");


$id = $_POST['ID'];
$book = new Book();

$books = $book->edit($id);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit your book Title</title>
        
    </head>
    <body style="text-align: center">
        <form action="Store.php" method="post">
            
            <h1>Editing form of Book Title.</h1>
            <fieldset>
                <legend>Edit Book Title</legend>
                <input name="title" value="<?php echo $books['title']?>"/><br/><br/>
                <input name="author" value="<?php echo $books['author']?>"><br/><br/>
                <input name="image" value="<?php echo $books['image']?>"><br/><br/>
                <input type="hidden" name="ID" value="<?php echo $books['ID']?>"><br/><br/>
            <button type="submit">Save</button>
            </fieldset>
            
        </form>
    </body>
</html>
