<?php

namespace App\BITM\SEIP106607\Message;

class Message {
    
    static public function set($message = ""){
        $_SESSION['Message']= $message;
    }
    
    static public function get($data=""){
        return $_SESSION['Message'];
    }

    static public function flash($data=""){
        $_Message= $_SESSION['Message'];
        $_SESSION['Message']= "";
        return $_Message;   
    }
}
