<?php

namespace App\BITM\SEIP106607\Utility;
class Uploader {
    
    static public function upload($files){
        $source = $files['tmp_name'];
        $destination = $_SERVER["DOCUMENT_ROOT"].DIRECTORY_SEPARATOR."ByMe".DIRECTORY_SEPARATOR."View".DIRECTORY_SEPARATOR."SEIP106607".DIRECTORY_SEPARATOR."Resources".DIRECTORY_SEPARATOR."Image".DIRECTORY_SEPARATOR;
        $destinationFileName = self::uniqueName($files['name']);
        $isUploaded = move_uploaded_file($source, $destination.$destinationFileName);
        
        return $isUploaded;
    }
    
    static private function uniqueName($filename){
        
        list($name,$ext) = explode(".", $filename);
        return $name."_".time().".".$ext;
    }
}
